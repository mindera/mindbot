FROM node:10.12.0-alpine

# not cool having this token here but this docker file is not used
ENV HUBOT_SLACK_TOKEN "blablabla"

ADD . /opt/bot
RUN cd /opt/bot && npm i

WORKDIR /opt/bot

CMD [ "/opt/bot/bin/hubot", "--adapter", "slack" ]
