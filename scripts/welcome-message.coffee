# Description:
#   Sends a DM to a user on joining #general channel
#
# Commands:
#   hubot welcome message - Show me the welcome message

GENERAL = "chatops"
WELCOME_MESSAGE = "Welcome!"

module.exports = (robot) ->

  robot.enter (res) ->
    channel = robot.adapter.client.rtm.dataStore.getChannelById(res.message.room).name
    if channel == GENERAL
      user = robot.adapter.client.rtm.dataStore.getUserByName(res.message.user.slack.name)
      robot.messageRoom user.id, WELCOME_MESSAGE

  robot.respond /welcome message/i, (res) ->
    user = robot.adapter.client.rtm.dataStore.getUserByName(res.message.user.slack.name)
    res.send WELCOME_MESSAGE
