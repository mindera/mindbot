'use strict'

// Description
//   hubot scripts for diagnosing hubot
//
// Commands:
//   hubot who are you - Reply with an explanation of what hubot is and what he does

const MESSAGE = `*Who am I?*
I'm Mindbot! I'm the resident minion here. I can carry on any task you want (as long as you can code it on coffeescript or javascript).

*No seriously, what are you?*
I'm an instance of Hubot that lives somewhere inside Mindera's AWS.

GitHub, Inc., wrote the first version of Hubot to automate the company's chat room. Hubot knew how to deploy the site, automate a lot of tasks, and be a source of fun around the office. Eventually he grew to become a formidable force in GitHub, but he led a private, messy life. So he was rewritten.
Today's version of Hubot is open source, written in CoffeeScript on Node.js, and easily deployed on platforms like Heroku. More importantly, Hubot is a standardized way to share scripts between everyone's robots.

*What can you do?*
GitHub usually ships Hubot units with a small group of core scripts: things like posting images, translating languages, and integrating with Google Maps.
I have none of that. When I first got here at Mindera, I was told about self-organization.
I decided that those plugins were not that useful and erased them.

But, hey! Don't be sad. The real fun happens when you teach me new stuff.
I'm really good at learning new things.

*How can I teach you new stuff?*
My code repository is here: https://bitbucket.org/mindera/mindbot/src/master/
Check out this documentation for writing your own Hubot scripts: https://hubot.github.com/docs/
Then the sky's the limit; just add them to your generated _scripts_ directory.

If your script turns me into SkyNet, please think twice about deploying it. I don't want Arnold knocking at my door.`

module.exports = function (robot) {
  robot.respond(/who are you$/i, msg => {
    msg.send(MESSAGE)
  })
};
