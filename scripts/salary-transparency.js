'use strict'

// Description
//   hubot script for giving info about salary transparency
//
// Commands:
//   hubot salary transparency - Reply with an explanation of where you can find more information about salary transparency

const MESSAGE = `*Salary transparency (at least for now) is only applicable to Mindera PT offices*
There is a group of people that share their salary amongst themselves.
If you wish to join them, ask in #salary-transparency to give you an onboarding (ask around for Pedro Vicente. He's the prettiest guy in that channel).
Take a look at this file to better understand how all of this works:
https://docs.google.com/document/d/11mymZY8qbv41Zs34EST3mft3zZBtjV98Q7I3RZuqed4/edit?usp=sharing`;

module.exports = function (robot) {
  robot.respond(/salary transparency$/i, msg => {
    msg.send(MESSAGE)
  })
};
